<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouponTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([20, 30, 45, 55] as $discount){

            DB::table('coupons')->insert([
                'code' =>md5('testing.' .$discount),
                'percent_off' =>$discount
            ]);
        }
    }
}
