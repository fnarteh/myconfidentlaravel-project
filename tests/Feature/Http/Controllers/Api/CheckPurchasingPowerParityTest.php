<?php

namespace Tests\Feature\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\URL;
use Mockery;
use Facades\App\Services\Geocoder;
use Tests\TestCase;

class CheckPurchasingPowerParityTest extends TestCase
{
    use  RefreshDatabase, WithFaker;

    
    /**
     *
     *
     * @test
     */
    public function it_returns_a_400_when_no_ip_address_is_provided ()
    { 
        
        $response = $this->withServerVariables(['REMOTE_ADDR' => null])
            ->get(route('api.purchasing-power'));

        $response->assertStatus(400);
    }


    /**
     *
     *
     * @test
     */
    public function it_returns_a_424_when_ip_address_can_not_be_geocoded()
    { 

        
        $ip =$this->faker->ipv4;

        $geocoder = Mockery::mock();

            $geocoder->shouldReceive('countryForIp')
                   ->with($ip)
                   ->andReturnNull();

            Geocoder::swap($geocoder);

        $response = $this->withServerVariables([
            'REMOTE_ADDR' => $ip])
            ->get(route('api.purchasing-power'));

        $response->assertStatus(424);
    }

     /**
     *
     *@test
     * 
     */
    public function it_returns_a_200_when_ip_address_for_country_without_ppp ()
    {
        
        $this->withoutExceptionHandling();
        $ip =$this->faker->ipv4;

        $geocoder = Mockery::mock();

            $geocoder->shouldReceive('countryForIp')
                   ->with($ip)
                   ->andReturn('Ghana');

            Geocoder::swap($geocoder);

        $response = $this->withServerVariables([
            'REMOTE_ADDR' => $ip])
            ->get(route('api.purchasing-power'));

        $response->assertStatus(200);
    }

     /** 
     *@test
     *@dataProvider pppCountriesDataProvider
     */
    public function it_returns_a_banner_when_country_has_ppp($country, $code, $percent_off)
    { 

        // $this->markTestSkipped();
        // $this->withoutExceptionHandling();
        $ip = $this->faker->ipv4;

        $this->artisan('db:seed', ['--class' =>'CouponTableSeeder']);

        $geocoder = Mockery::mock();

            $geocoder->shouldReceive('countryForIp')
                   ->with($ip)
                   ->andReturn($country);

            Geocoder::swap($geocoder);

            $now = now();
            Carbon::setTestNow($now);

            $url = URL::temporarySignedRoute('purchasing-power', now()->addMinutes(3), ['code' => $code]);

            // dd($url);

        $response = $this->withServerVariables([
            'REMOTE_ADDR' => $ip])
            ->get(route('api.purchasing-power'));
            //  dd($response);
 
        $response->assertStatus(200);
        $response->assertViewIs('partials.ppp-banner');
        $response->assertSee('action="'.$url.'"');
        $response->assertSee($country );
        $response->assertSee($percent_off.'% discount' );
    }

    public  function  pppCountriesDataProvider(){
        return[
            ['India', '51d2e2e98364d1e6442e98a34f7f6b70', 55],
            ['Ghana', '51d2e2e98364d1e6442e98a34f7f6b70', 55],
            ['Hungary', '465c59e9eacbf418a03d2a7799c4f857', 45],
            ['Spain ', 'e8770590be811a9024dc67c27ecfc6f6', 30],
            ['France', 'd1250579891d96b718fafc6f23e2f703', 20],
            
        
        ];
    }
}
