

<aside id="pppBanner" class="w-full bg-red-500 border-b border-t border-red-700">
    <div class="max-w-3xl mx-auto py-12 px-6">
        <form method="post" action="{!! $url !!}" class="text-red-100 text-lg leading-normal">
            <input type="hidden" name="_token" value="[csrf-token]">
            <p>I see you are visiting from <span class="font-semibold">{{ $country }}</span> where <b>Confident Laravel</b> might be more expensive. I want to make sure this video course is <b>affordable for everyone</b> so you can start testing your Laravel applications.</p>
            <p class="mt-4">I support <a href="https://en.wikipedia.org/wiki/Purchasing_power_parity" rel="external" target="_blank" class="underline hover:text-red-200">Purchasing Power Parity</a> which allows you to buy <b>Confident Laravel</b> at a <button type="submit" class="underline hover:text-red-200">{{ $percentage_off}}% discount</button>.</p>
        </form>
    </div>
</aside>
