<?php


namespace App\Models;

use App\Models\Product;
use App\PurchasingPower;
use App\Models\User;
use  App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    public function wasAlreadyUsed(User $user = null)
    {
        if (!$user) {
            return false;
        }

        return Order::where('user_id', $user->id)->where('coupon_id', $this->id)->exists();
    }


    public static function  findByCountry($country){
        $factor = PurchasingPower::factorForCountry($country);

        if(is_null($factor)){
            return null;
        }



        $code = 'd1250579891d96b718fafc6f23e2f703';

        if($factor < .3){
            $code = '51d2e2e98364d1e6442e98a34f7f6b70';
        }elseif($factor < .45){
            $code = '465c59e9eacbf418a03d2a7799c4f857';
        }elseif($factor < .6){
            $code = 'e8770590be811a9024dc67c27ecfc6f6';
        }
        return Coupon::where('code', $code)->first();
    }

    public function price(Product $product)
    {
        return $this->apply($product->price);
    }

    public function priceInCents(Product $product)
    {
        return $this->apply($product->priceInCents());
    }

    private function apply($amount)
    {
        return $amount * ((100 - $this->percent_off) / 100);
    }
}
