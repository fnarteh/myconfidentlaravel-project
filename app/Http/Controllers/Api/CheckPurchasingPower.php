<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Facades\App\Services\Geocoder;

class CheckPurchasingPower extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if (empty($request->server('REMOTE_ADDR'))){
            abort(400);
        }
        $country = Geocoder::countryForIp($request->server('REMOTE_ADDR'));
            if(is_null($country)){
                abort(424);
            }

            $coupon = Coupon::findByCountry($country);
            if(is_null($coupon )){
                return response()->noContent(200);
            }

            return view('partials.ppp-banner',[
                'country' => $country,
                'url' => URL::temporarySignedRoute('purchasing-power', now()->addMinutes(3), ['code' => $coupon->code]),
                'percentage_off' => $coupon->percent_off ,
            ]);

        }





}
