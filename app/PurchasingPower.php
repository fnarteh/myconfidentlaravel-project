<?php

namespace App;

class PurchasingPower{


    private static $factors = [
        'India' => 0.28454 ,
        'Ghana'=>  0.28449,
        'Hungary' => 0.44674,
        'Spain ' => 0.58353 ,
        'France' => 0.75743,
    ];


    public static function factorForCountry($country ){

        return self::$factors[$country] ?? null;

    }
}
